module.exports = {
  root: true,
  parser: "@typescript-eslint/parser",
  parserOptions: { project: ["./tsconfig.json"] },
  extends: [
    "plugin:@typescript-eslint/recommended",
    "plugin:prettier/recommended",
  ],
  ignorePatterns: [
    "build/**/*",
    "dist/**/*",
    "out/**/*",
    "public/**/*",
    "resources/**/*",
  ],
  overrides: [
    {
      files: ["src/**/*.ts"],
    },
  ],
};
