#!/usr/bin/env bash

set -Eeuo pipefail

export PATH="${PATH}:${ROOT}/bin"

if [ $# -ne 0 ] ; then
    exec bash -c "$@"
else
    exec bash --login
fi
