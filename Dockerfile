FROM node:current-slim

ARG APT_CACHE_DIR=/opt/cache/apt \
    NPM_CACHE_DIR=/opt/cache/npm

# RUN_WKHTMLTOPDF   if true, use wkhtmltopdf to convert to pdf, overwriting puppeteer's output
# SITE              if set, copy site to that dir
# OUTPUT            if set, copy output to that dir
# TEST_INPUT        if set, retrieve the input files from that dir
ENV ROOT=/opt/resume \
    XDG_RUNTIME_DIR=/tmp/runtime-root \
    RUN_WKHTMLTOPDF=true \
    SITE= \
    OUTPUT= \
    PRIVATE_REPO= 

RUN mkdir -p $APT_CACHE_DIR $NPM_CACHE_DIR

RUN --mount=type=cache,target=${APT_CACHE_DIR} \
    apt-get update -y && \
    apt-get -o dir::cache::archives="${APT_CACHE_DIR}" install -y \
        wkhtmltopdf libnss3-dev libgdk-pixbuf2.0-dev libgtk-3-dev libxss-dev libasound-dev rsync parallel zip unzip curl 

COPY . ${ROOT}
RUN chmod +x ${ROOT}/entrypoint.sh ${ROOT}/bin/*

WORKDIR ${ROOT}

RUN --mount=type=cache,target=${NPM_CACHE_DIR} \
    npm set cache ${NPM_CACHE_DIR} && \
    npm ci --loglevel verbose

RUN npm run-script lint:fix && npm run-script build

ENTRYPOINT [ "./entrypoint.sh" ]
CMD