import { Document, HeadingLevel, Packer, Paragraph, TextRun } from "docx";
import { FileChild } from "docx/build/file/file-child";
import { ResumeSchema } from "../generated/resume.js";

function makeHeader(resume: ResumeSchema): FileChild[] {
  return [
    new Paragraph({
      heading: HeadingLevel.HEADING_1,
      text: resume.basics?.name,
    }),
    new Paragraph({
      text: `${resume.basics?.location?.city}, ${resume.basics?.location?.countryCode}`,
    }),
    new Paragraph({
      text: resume.basics?.email,
    }),
    new Paragraph({
      text: resume.basics?.phone,
    }),
    new Paragraph({
      text: resume.basics?.url,
    }),
    new Paragraph(""),
  ];
}

function makeWorkSection(resume: ResumeSchema): FileChild[] {
  const workSection =
    resume.work?.flatMap((job) => [
      new Paragraph({
        heading: HeadingLevel.HEADING_3,
        text: job.name,
      }),
      new Paragraph({
        text: job.position,
      }),
      new Paragraph({
        text: `${job.startDate} - ${job.endDate ?? "current"}`,
      }),
      ...(job.highlights?.map(
        (text) =>
          new Paragraph({
            bullet: {
              level: 0,
            },
            text,
          }),
      ) ?? []),
      new Paragraph(""),
    ]) ?? [];

  return [
    new Paragraph({
      heading: HeadingLevel.HEADING_2,
      text: "Employment",
    }),
    ...workSection,
    new Paragraph(""),
  ];
}

function makeProjectsSection(resume: ResumeSchema): FileChild[] {
  if (resume.projects === undefined) return [];

  return [
    new Paragraph({
      text: "Projects",
      heading: HeadingLevel.HEADING_2,
    }),
    ...resume.projects.flatMap((project) => [
      new Paragraph({
        text: project.name,
        heading: HeadingLevel.HEADING_3,
      }),
      new Paragraph({
        text:
          project.startDate === undefined
            ? undefined
            : `${project.startDate}-${project.endDate ?? "ongoing"}`,
      }),
      new Paragraph({
        text: project.description,
      }),
      ...(project.highlights === undefined
        ? []
        : project.highlights.map(
            (hightlight) =>
              new Paragraph({
                text: hightlight,
                bullet: {
                  level: 0,
                },
              }),
          )),
      ...(project.roles === undefined
        ? []
        : [
            new Paragraph({
              children: [
                new TextRun({
                  text: "Roles: ",
                  bold: true,
                }),
                new TextRun({
                  text: project.roles?.join(", "),
                }),
              ],
            }),
          ]),
      ...(project.keywords === undefined
        ? []
        : [
            new Paragraph({
              children: [
                new TextRun({
                  text: "Keywords: ",
                  bold: true,
                }),
                new TextRun({
                  text: project.keywords?.join(", "),
                }),
              ],
            }),
          ]),
      new Paragraph(""),
    ]),
  ];
}

function makeSkillsSection(resume: ResumeSchema): FileChild[] {
  return resume.skills !== undefined
    ? [
        new Paragraph({
          text: "Skills",
          heading: HeadingLevel.HEADING_2,
        }),
        ...resume.skills?.map(
          (skill) =>
            new Paragraph({
              children: [
                new TextRun({
                  text: skill.name + " ",
                  bold: true,
                }),
                // new TextRun({
                //   text: skill.level !== undefined ? `(${skill.level}): ` : ": ",
                // }),
                new TextRun({
                  text: skill.keywords?.join(", "),
                }),
              ],
              bullet: {
                level: 0,
              },
            }),
        ),
      ]
    : [];
}

export type DocxSectionRenderer = (resume: ResumeSchema) => FileChild[];

export async function renderDocx(
  resume: ResumeSchema,
  ...sectionRenderers: DocxSectionRenderer[]
): Promise<Buffer> {
  const doc = new Document({
    sections: [
      {
        properties: {},
        children: sectionRenderers.flatMap((f) => f(resume)),
      },
    ],
  });

  return Packer.toBuffer(doc);
}

export async function renderShortDocx(resume: ResumeSchema): Promise<Buffer> {
  return renderDocx(resume, makeHeader, makeWorkSection, makeSkillsSection);
}

export async function renderLongDocx(resume: ResumeSchema): Promise<Buffer> {
  return renderDocx(
    resume,
    makeHeader,
    makeWorkSection,
    makeProjectsSection,
    makeSkillsSection,
  );
}
