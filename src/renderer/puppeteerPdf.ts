import { ResumeSchema } from "../generated/resume.js";
import { Renderer } from "../types.js";
import { Browser, PDFOptions } from "puppeteer";

export type PuppeteerPdfRendererOptions = PDFOptions & {
  emulateMediaType?: string;
};

export class PuppeteerPdfRenderer {
  constructor(private readonly browser: Browser) {}

  wrapHtmlRenderer(
    renderHtml: Renderer,
    opts?: PuppeteerPdfRendererOptions,
  ): Renderer {
    const browser = this.browser;

    return async function (resume: ResumeSchema): Promise<Buffer> {
      const htmlContent = await (async () => {
        const content = await renderHtml(resume);
        if (typeof content === "string") {
          return content;
        }
        return content.toString("utf-8");
      })();

      const page = await browser.newPage();

      await page.setContent(htmlContent, {
        waitUntil: "networkidle0",
      });

      if (opts?.emulateMediaType !== undefined) {
        await page.emulateMediaType(opts.emulateMediaType);
      }

      return page
        .pdf({
          format: "LETTER",
          ...(opts ?? {}),
        })
        .then(Buffer.from);
    };
  }
}
