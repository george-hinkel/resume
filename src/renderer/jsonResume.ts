import { ResumeSchema } from "../generated/resume.js";
import { Renderer } from "../types.js";
import { render } from "resumed";

export type Theme = Parameters<typeof render>[1];

export function rendererForTheme(theme: Theme): Renderer {
  return (resume: ResumeSchema) => render(resume, theme);
}
