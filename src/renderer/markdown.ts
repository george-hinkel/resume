import { PathOrFileDescriptor, readFileSync } from "fs";
import mustache from "mustache";
import { unified } from "unified";
import markdown from "remark-parse";
import remark2rehype from "remark-rehype";
import doc from "rehype-document";
import format from "rehype-format";
import html from "rehype-stringify";
import gfm from "remark-gfm";
import less from "less";
import { ResumeSchema } from "../generated/resume.js";
import { Renderer } from "../types.js";

export class MarkdownRenderer {
  private constructor(
    private readonly mustacheTemplate: string,
    private readonly css: string,
  ) {}

  static async create(props: {
    mustacheTemplateFile: PathOrFileDescriptor;
    lessFile: PathOrFileDescriptor;
  }): Promise<MarkdownRenderer> {
    const { mustacheTemplateFile, lessFile } = props;
    const mustacheTemplate =
      readFileSync(mustacheTemplateFile).toString("utf-8");
    const lessStyle = readFileSync(lessFile).toString("utf-8");
    const renderedLess = await less.render(lessStyle);
    const css = renderedLess.css;
    return new MarkdownRenderer(mustacheTemplate, css);
  }

  renderMarkdown(resume: ResumeSchema): string {
    const regexp = new RegExp(/(\d{4})-(\d{2})-(\d{2})/, "g");
    const replacedResume = JSON.parse(
      JSON.stringify(resume).replaceAll(regexp, "$2/$1"),
    );
    return mustache.render(this.mustacheTemplate, replacedResume);
  }

  markdownRenderer(): Renderer {
    return (resume: ResumeSchema) => this.renderMarkdown(resume);
  }

  async renderMarkdownHtml(resume: ResumeSchema): Promise<string> {
    const resumeMarkdown = this.renderMarkdown(resume);
    return unified()
      .use(markdown)
      .use(gfm)
      .use(remark2rehype)
      .use(doc, { style: this.css })
      .use(format)
      .use(html)
      .processSync(resumeMarkdown)
      .toString("utf-8");
  }

  markdownHtmlRenderer(): Renderer {
    return (resume: ResumeSchema) => this.renderMarkdownHtml(resume);
  }
}
