import { resolve } from "path";
import * as evenTheme from "jsonresume-theme-even";
// @ts-expect-error no type defs for theme, but not needed
import * as classTheme from "@jsonresume/jsonresume-theme-class";
// @ts-expect-error no type defs for theme, but not needed
import * as standardResumeTheme from "jsonresume-theme-standard-resume";
// @ts-expect-error no type defs for theme, but not needed
import * as eloquentTheme from "jsonresume-theme-eloquent";
import { renderLongDocx, renderShortDocx } from "./renderer/docx.js";
import {
  changeResumeEmail,
  changeResumeUrl,
  rendererWithPreprocessing,
  removeMarkdownFromResume,
  loadResume,
  loadYaml,
} from "./util.js";
import { MarkdownRenderer } from "./renderer/markdown.js";
import { readFile } from "fs/promises";
import { rendererForTheme } from "./renderer/jsonResume.js";
import { OutputWriter } from "./outputWriter.js";
import { PuppeteerPdfRenderer } from "./renderer/puppeteerPdf.js";
import { launch } from "puppeteer";
import { ResumeSchema } from "./generated/resume.js";

function modifyResumeForCompany(
  resume: ResumeSchema,
  company: string,
): ResumeSchema {
  return changeResumeUrl(
    changeResumeEmail(resume, `${company}@mail.ghinkel.dev`),
    `https://resume.ghinkel.dev/${company}`,
  );
}

async function main() {
  const resumeSourceDir = "resume_source";
  const resourceDir = "resources";
  const outputDir = "out";
  const siteDir = resolve(outputDir, "public");

  const markdownRenderer = await MarkdownRenderer.create({
    mustacheTemplateFile: resolve(resourceDir, "markdown_resume.mustache"),
    lessFile: resolve(resourceDir, "markdown_resume.less"),
  });

  const browser = await launch({
    args: ["--no-sandbox"],
  });

  const puppeteerPdfRenderer = new PuppeteerPdfRenderer(browser);

  const standardResumeRenderer = rendererWithPreprocessing(
    rendererForTheme(standardResumeTheme),
    removeMarkdownFromResume,
    (resume) => {
      return {
        ...resume,
        languages: undefined,
      };
    },
  );

  const eloquentRenderer = rendererWithPreprocessing(
    rendererForTheme(eloquentTheme),
    removeMarkdownFromResume,
  );

  const pdfRenderer = puppeteerPdfRenderer.wrapHtmlRenderer(
    standardResumeRenderer,
    {
      scale: 0.8,
    },
  );

  const shortDocxRenderer = rendererWithPreprocessing(
    renderShortDocx,
    removeMarkdownFromResume,
  );

  const longDocxRenderer = rendererWithPreprocessing(
    renderLongDocx,
    removeMarkdownFromResume,
  );

  const outputWriter = new OutputWriter({
    outputDir,
    siteDir,
    siteRenderers: [
      ["index.html", rendererForTheme(evenTheme)],
      ["resume-puppeteer.pdf", pdfRenderer],
      ["short.docx", shortDocxRenderer],
      ["long.docx", longDocxRenderer],
      ["even.html", rendererForTheme(evenTheme)],
      ["class.html", rendererForTheme(classTheme)],
      ["standard-resume.html", standardResumeRenderer],
      ["standard-resume-puppeteer.pdf", pdfRenderer],
      ["eloquent.html", eloquentRenderer],
      ["download.html", () => readFile(resolve(resourceDir, "download.html"))],
    ],
    outputRenderers: [
      ["resume.md", markdownRenderer.markdownRenderer()],
      ["resume.md.html", markdownRenderer.markdownHtmlRenderer()],
    ],
  });

  const generateDirectives =
    (loadYaml(resolve(resumeSourceDir, "generate.yml")) as {
      [k: string]: string;
    }) ?? {};

  return Promise.all([
    outputWriter.renderAndWrite(
      loadResume(resolve(resumeSourceDir, "resume.yml")),
    ),
    ...Object.entries(generateDirectives).map(([company, resumePath]) =>
      outputWriter
        .withPrefix(company)
        .renderAndWrite(
          modifyResumeForCompany(
            loadResume(resolve(resumeSourceDir, resumePath)),
            company,
          ),
        ),
    ),
  ]).then(() => browser.close());
}

await main();
