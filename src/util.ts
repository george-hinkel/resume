import { PathOrFileDescriptor, readFileSync } from "fs";
import { load } from "js-yaml";
import { resolve } from "path";
import { ResumeSchema } from "./generated/resume.js";
import JSONSchema from "json-schema-library";
import { unified } from "unified";
import remarkParse from "remark-parse";
import remarkGfm from "remark-gfm";
import remarkStringify from "remark-stringify";
import stripMarkdown from "strip-markdown";
import { RenderedOutput, Renderer } from "./types.js";

const schemaPath = resolve("resources", "schema.json");

export function loadYaml(
  path: PathOrFileDescriptor,
  opts?: Parameters<typeof load>[1],
): unknown {
  const buffer = readFileSync(path);
  const content = buffer.toString("utf-8");
  return load(content, opts);
}

export function loadResume(path: PathOrFileDescriptor): ResumeSchema {
  const resumeObject = loadYaml(path);

  const schemaObject = JSON.parse(readFileSync(schemaPath).toString("utf-8"));

  const schema = new JSONSchema.Draft04(schemaObject);

  const errors = schema.validate(resumeObject).filter((error) => {
    if (error.data.pointer === "#/meta/lastModified") {
      return false;
    }

    return true;
  });

  if (errors.length > 0) {
    throw new Error(JSON.stringify(errors));
  }

  return resumeObject as ResumeSchema;
}

export function removeMarkdownFromResume(resume: ResumeSchema): ResumeSchema {
  const processor = unified()
    .use(remarkParse)
    .use(remarkGfm)
    .use(stripMarkdown)
    .use(remarkStringify)
    .freeze();

  function clean(text: string): string {
    return processor.processSync(text).toString("utf-8");
  }

  return {
    ...resume,
    work: resume.work?.map((job) => {
      return {
        ...job,
        summary: job.summary !== undefined ? clean(job.summary) : undefined,
        highlights: job.highlights?.map(clean),
      };
    }),
  };
}

export function changeResumeEmail(
  resume: ResumeSchema,
  newEmail: string,
): ResumeSchema {
  return {
    ...resume,
    basics:
      resume.basics === undefined
        ? undefined
        : {
            ...resume.basics,
            email: newEmail,
          },
  };
}

export function changeResumeUrl(
  resume: ResumeSchema,
  newUrl: string,
): ResumeSchema {
  return {
    ...resume,
    basics:
      resume.basics === undefined
        ? undefined
        : {
            ...resume.basics,
            url: newUrl,
          },
  };
}

export type ResumePreprocessor = (resume: ResumeSchema) => ResumeSchema;

export function rendererWithPreprocessing(
  renderer: Renderer,
  ...preprocessors: ResumePreprocessor[]
): Renderer {
  return function (resume: ResumeSchema): RenderedOutput {
    return renderer(
      preprocessors.reduce(
        (currentResume, processor) => processor(currentResume),
        resume,
      ),
    );
  };
}
