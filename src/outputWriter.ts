import { resolve } from "path";
import { ResumeSchema } from "./generated/resume.js";
import { mkdir, writeFile } from "fs/promises";
import { RenderedOutput, RendererDefinition } from "./types.js";

export class OutputWriter {
  private readonly outputDir: string;
  private readonly siteDir: string;
  private readonly prefix: string;
  private readonly siteRenderers: RendererDefinition[];
  private readonly outputRenderers: RendererDefinition[];

  constructor(props: {
    outputDir: string;
    siteDir: string;
    prefix?: string;
    siteRenderers?: RendererDefinition[];
    outputRenderers?: RendererDefinition[];
  }) {
    this.outputDir = props.outputDir;
    this.siteDir = props.siteDir;
    this.prefix = props.prefix ?? ".";
    this.siteRenderers = props.siteRenderers ?? [];
    this.outputRenderers = props.outputRenderers ?? [];
  }

  public withPrefix(prefix: string): OutputWriter {
    return new OutputWriter({
      outputDir: this.outputDir,
      siteDir: this.siteDir,
      prefix,
      siteRenderers: this.siteRenderers,
      outputRenderers: this.outputRenderers,
    });
  }

  public async renderAndWrite(resume: ResumeSchema): Promise<void> {
    await Promise.all(
      [this.outputDir, this.siteDir].map((baseDir) =>
        mkdir(resolve(baseDir, this.prefix), {
          recursive: true,
        }),
      ),
    ).then(() =>
      Promise.all([
        ...this.siteRenderers.map((renderer) => {
          const [filename, render] = renderer;
          return this.write(this.siteDir, filename, render(resume));
        }),
        ...this.outputRenderers.map((renderer) => {
          const [filename, render] = renderer;
          return this.write(this.outputDir, filename, render(resume));
        }),
      ]),
    );
  }

  private async write(
    baseDir: string,
    filename: string,
    content: RenderedOutput,
  ) {
    return Promise.resolve(content).then((resolvedContent) =>
      writeFile(resolve(baseDir, this.prefix, filename), resolvedContent),
    );
  }
}
