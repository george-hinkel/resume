import { ResumeSchema } from "./generated/resume.js";

export type RenderedOutput =
  | string
  | Buffer
  | Promise<string>
  | Promise<Buffer>;
export type Renderer = (resume: ResumeSchema) => RenderedOutput;
export type FileName = string;
export type RendererDefinition = [FileName, Renderer];
