# George Hinkel's Resume CI/CD Repo

This repository contains a gitlab CI/CD pipeline to deploy updates to my resume automatically, utilizing the json-resume CLI to generate a static site and staticrypt to encrypt it with a password in order to control who sees my resume with a passphrase. The theme for my resume can be found on the NPM registry by the name of jsonresume-theme-elegant. Use any of this at will

## Deployed Versions

* [Static Site](https://resume.georgehinkel.com)

## Forking and using for your own resume

In order to use this you must:
1. Create a resume.json file conforming to the jsonresume schema which can be found on github
2. Store it in a separate private repository on gitlab
3. Create a Personal Access Token with Read Repository priveleges
4. Add variables to this project with the project ID for the private repo, and with the Personal Access Token
5. [Optional] add CI/CD to your private repo to trigger this pipeline when it is updated
6. Enjoy!
